# disaster tweets

Disaster tweets NLP model trainng

## Trained with SVC

           precision    recall  f1-score   support

           0       0.79      0.91      0.84      1312
           1       0.85      0.66      0.74       972

    accuracy                           0.81      2284
   macro avg       0.82      0.79      0.79      2284
weighted avg       0.81      0.81      0.80      2284


[[1195  117]
 [ 326  646]]


## Trained with Logistic Regression

              precision    recall  f1-score   support

           0       0.80      0.86      0.83      1312
           1       0.79      0.71      0.75       972

    accuracy                           0.80      2284
   macro avg       0.80      0.79      0.79      2284
weighted avg       0.80      0.80      0.80      2284


[[1132  180]
 [ 278  694]]
